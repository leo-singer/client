<!-- For the contact information below, please enter
     your name and email that was used to register with
     SCiMMA -->

#### Request Details:
- **Requestor Name:** <!-- insert name -->
- **Requestor Email:** <!-- insert email address -->
- **SCiMMA ID:** <!-- inserct SCiMMA ID (SCiMMA1000XXXX) -->

#### Requestor Statement: 

By submitting this request, I am confirming that my information above is accurate, and I that I am currently an active member of LIGO/Virgo/KAGRA. As such, I am authorized to receive internal LVK alerts from GraceDB. 

### Request:

Please add the above user to the `lvk-users` group on `kafka.scimma.org` to allow access to topics from GraceDB (Production, Playground, and Test).
